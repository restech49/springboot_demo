package com.restech.springdemo.converter;

import com.restech.springdemo.dto.SkillDto;
import com.restech.springdemo.entity.Skill;

public class SkillConverter {

	public static Skill dtoToEntity(SkillDto SkillDto) {
		Skill Skill = new Skill(SkillDto.getSkillName(), null);
		Skill.setSkillId(SkillDto.getSkillId());
		return Skill;
	}


	public static SkillDto entityToDto(Skill skill) {
		return new SkillDto(skill.getSkillId(), skill.getSkillName());
	}
}
