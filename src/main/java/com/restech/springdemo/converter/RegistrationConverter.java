package com.restech.springdemo.converter;

import java.util.stream.Collectors;

import com.restech.springdemo.dto.UserDto;
import com.restech.springdemo.dto.UserRegistrationDto;
import com.restech.springdemo.entity.Registration;
import com.restech.springdemo.entity.User;

public class RegistrationConverter {
	 public static Registration dtoToEntity(UserRegistrationDto userRegistrationDto) {
	  Registration user = new Registration(userRegistrationDto.getUserFirstName(), userRegistrationDto.getUserLastName(),userRegistrationDto.getUserPassword(),userRegistrationDto.getUserConfirmPassword(),null);
	  user.setUserId(userRegistrationDto.getUserId());
	  user.setSkills(userRegistrationDto.getSkillDtos().stream().map(SkillConverter::dtoToEntity).collect(Collectors.toList()));
	  return user;
	 }
	 public static UserRegistrationDto entityToDto(Registration userRegistration) {
	  UserRegistrationDto userRegistrationDto = new UserRegistrationDto(userRegistration.getUserId(), userRegistration.getUserFirstName(),userRegistration.getUserLastName(),userRegistration.getUserPassword(),userRegistration.getUserConfirmPassword(), null);
	  userRegistrationDto.setSkillDtos(userRegistration.getSkills().stream().map(SkillConverter::entityToDto).collect(Collectors.toList()));
	  return userRegistrationDto;
	 }
	}