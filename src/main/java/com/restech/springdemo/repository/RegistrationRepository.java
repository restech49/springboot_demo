package com.restech.springdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.restech.springdemo.entity.Registration;
import com.restech.springdemo.entity.User;


/**
 * For DB operations, use SpringData JPA
 * @author KUMA17
 *
 */

@Repository
public interface RegistrationRepository extends JpaRepository<Registration, Integer>{
}

