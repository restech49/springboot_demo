package com.restech.springdemo.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restech.springdemo.converter.RegistrationConverter;
import com.restech.springdemo.converter.UserConverter;
import com.restech.springdemo.dto.UserDto;
import com.restech.springdemo.dto.UserRegistrationDto;
import com.restech.springdemo.repository.RegistrationRepository;
import com.restech.springdemo.repository.UserRepository;



@Service
public class UserServiceimpl implements UserService {
 @Autowired
 UserRepository userRepository;
 
 @Autowired
 RegistrationRepository registrationRepository;
 
 @Override
 public UserDto getUserById(Integer userId) {
	 System.out.println("I am inside getUserById of user service impl");
  return UserConverter.entityToDto(userRepository.getOne(userId));
  
 }
 @Override
 public void saveUser(UserDto userDto) {
  userRepository.save(UserConverter.dtoToEntity(userDto));
  System.out.println("User data save");
 }
 @Override
 public List <UserRegistrationDto> getAllUsers() {
	 System.out.println("inside get all users user service imp");	 
  return registrationRepository.findAll().stream().map(RegistrationConverter::entityToDto).collect(Collectors.toList());
 }
@Override
public UserRegistrationDto getUserByIdRegistration(Integer userId) {
	System.out.println("I am inside getUserById of user service impl");
	  return RegistrationConverter.entityToDto(registrationRepository.getOne(userId));
}
@Override
public void registerUser(UserRegistrationDto userDto) {
	// TODO Auto-generated method stub
	System.out.println("Inside register user service implementation");
	registrationRepository.save(RegistrationConverter.dtoToEntity(userDto));
	  System.out.println("User data registered");
}

}
