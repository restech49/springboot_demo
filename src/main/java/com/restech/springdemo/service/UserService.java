package com.restech.springdemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restech.springdemo.dto.UserDto;
import com.restech.springdemo.dto.UserRegistrationDto;


public interface UserService {
	
	UserDto getUserById(Integer userId);
	void  saveUser(UserDto userDto);
	List <UserRegistrationDto> getAllUsers();
	UserRegistrationDto getUserByIdRegistration(Integer userId);
	void  registerUser(UserRegistrationDto userDto);
	
	
}
