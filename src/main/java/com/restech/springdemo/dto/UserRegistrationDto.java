package com.restech.springdemo.dto;

import java.util.ArrayList;
import java.util.List;


/*****
 * In a typical web application, 
 * there are generally two types of data objects: DTO (to communicate through the client)
 * and Entity (to communicate through the DB).
 * @author KUMA17
 *
 */

public class UserRegistrationDto {
    Integer userId;
    String userFirstName;
    String userLastName;
    String userPassword;
    String userConfirmPassword;
    List<SkillDto> skillDtos= new ArrayList<>();
    
    public UserRegistrationDto(Integer userId, String userFirstName, String userLastName,String userPassword, String userConfirmPassword, List<SkillDto> skillDtos) {
        this.userId = userId;
        this.userFirstName = userFirstName;
        this.userLastName=userLastName;
        this.userPassword=userPassword;
        this.userConfirmPassword=userConfirmPassword;
        this.skillDtos = skillDtos;
    }
    public UserRegistrationDto() {
    }
    
    
    
    public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserLastName() {
		return userLastName;
	}
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getUserConfirmPassword() {
		return userConfirmPassword;
	}
	public void setUserConfirmPassword(String userConfirmPassword) {
		this.userConfirmPassword = userConfirmPassword;
	}
	
	public String getUserFirstName() {
		return userFirstName;
	}
	
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
    public List<SkillDto> getSkillDtos() {
        return skillDtos;
    }
    public void setSkillDtos(List<SkillDto> skillDtos) {
        this.skillDtos = skillDtos;
    }
}
