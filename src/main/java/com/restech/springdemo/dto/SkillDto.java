package com.restech.springdemo.dto;

/*****
 * In a typical web application, 
 * there are generally two types of data objects: DTO (to communicate through the client)
 * and Entity (to communicate through the DB).
 * @author KUMA17
 *
 */


public class SkillDto {
    Integer skillId;
    String SkillName;
    public SkillDto(Integer skillId, String skillName) {
        this.skillId = skillId;
        SkillName = skillName;
    }
    public SkillDto() {
    }
    public Integer getSkillId() {
        return skillId;
    }
    public void setSkillId(Integer skillId) {
        this.skillId = skillId;
    }
    public String getSkillName() {
        return SkillName;
    }
    public void setSkillName(String skillName) {
        SkillName = skillName;
    }
}