package com.restech.springdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.restech.springdemo.repository.RegistrationRepository;
import com.restech.springdemo.repository.UserRepository;





@SpringBootApplication
public class SpringdemoApplication {

	@Autowired
	UserRepository  userRepository;
	
	@Autowired
	RegistrationRepository  registrationRepository;
	
	
	public static void main(String[] args) {
		SpringApplication.run(SpringdemoApplication.class, args);
	}
}
