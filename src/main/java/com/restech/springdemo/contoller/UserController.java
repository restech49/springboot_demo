package com.restech.springdemo.contoller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.restech.springdemo.dto.UserDto;
import com.restech.springdemo.dto.UserRegistrationDto;
import com.restech.springdemo.service.UserService;


@RequestMapping("/user")
@RestController
public class UserController {

	@Autowired
	UserService userService;
		
	/*@RequestMapping(value= "{userId}")
	public UserDto getUserById(@PathVariable Integer userId) {
		System.out.println("I am inside integer request mapping");
		return userService.getUserById(userId);
	}*/
	
	@RequestMapping("/getAllUsers")
	 public List < UserRegistrationDto > getAllUsers() {
		System.out.println("getallusers usercontroller java");
		
		System.out.println(userService.getAllUsers());
		return userService.getAllUsers();
	 }
	
	/* @RequestMapping(value = "{userDto}", method = RequestMethod.POST)
	 public void saveUser(@RequestBody UserDto userDto) {
	  userService.saveUser(userDto);
	 }*/
	 
	 @RequestMapping(value = "{userDto}",method = RequestMethod.POST)
	 public void registerUser(@RequestBody UserRegistrationDto userRegistration) {
		 System.out.println("I am inside user contrroller user registration");
	  userService.registerUser(userRegistration);
	 }
	 
	 @RequestMapping(value= "{userId}",method=RequestMethod.GET)
		public UserDto getUserById(@PathVariable Integer userId) {
		 System.out.println("I am inside string request mapping");
			return userService.getUserById(userId);
		}
	 
}
