'use strict'
angular.module('demo.services', []).factory('UserService', ["$http", "CONSTANTS", function($http, CONSTANTS) {
    var service = {};
    service.getUserById = function(userId) {
        var url = CONSTANTS.getUserByIdUrl + userId;
        return $http.get(url);
    }
    service.getAllUsers = function() {
        return $http.get(CONSTANTS.getAllUsers);
    }
    service.saveUser = function(userDto) {
        return $http.post(CONSTANTS.saveUser, userDto);
    }
    
    service.registerUser = function(userDto) {
    	console.log("inside userservice.js")
    	return $http.post(CONSTANTS.registerUser, userDto);
    }
    
    
    service.fetchUsers = function() {
    	console.log("inside userservice.js fetch users")
        return $http.get(CONSTANTS.getAllUsers);
    }
    
    return service;
}]);