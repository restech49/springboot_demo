'use strict'
var demoApp = angular.module('demo', ['ui.bootstrap', 'demo.controllers','demo.controllers2','demo.services']);
console.log("i am inside app.js")
demoApp.constant("CONSTANTS", {
    getUserByIdUrl: "/user/getUser/",
    getAllUsers: "/user/getAllUsers",
    saveUser: "/user/saveUser",
    registerUser: "/user/registerUser"
});